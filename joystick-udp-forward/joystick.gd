extends Node

# https://docs.godotengine.org/en/stable/classes/class_packetpeerudp.html
var udp := PacketPeerUDP.new()
var connected = false

func _ready():
	udp.set_broadcast_enabled(true)
#	udp.set_dest_address("255.255.255.255", 4210)
	udp.connect_to_host("192.168.129.70", 4210)
	Input.start_joy_vibration(0, 1, 1, 1)

func _process(delta):
	var forward = Input.get_action_strength("drive_forward");
	var backward = Input.get_action_strength("drive_backward")
	var drive = forward - backward
	var right = Input.get_action_strength("turn_right");
	var left = Input.get_action_strength("turn_left");
	var turn = right - left
	$drive_forward.text = String(forward)
	$drive_backward.text = String(backward)
	$turn_left.text = String(left)
	$turn_right.text = String(right)
	udp.put_packet(("drive:" + String(drive)).to_utf8())
	udp.put_packet(("turn:" + String(turn)).to_utf8())
